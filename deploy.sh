export VERSION=$(cat version)
export STACKNAME="lnxchallenge"

docker stack deploy -c docker-compose.deploy.yml $STACKNAME
cd load-balancer
docker stack deploy -c docker-compose.nginx.yml $STACKNAME-proxy
