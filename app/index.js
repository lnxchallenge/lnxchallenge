var cluster = require('cluster');

if (cluster.isMaster){

    var cpuCount = require('os').cpus().length;
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

}else {

    var express = require('express');
    var app = express();
    app.get('/', function (req, res) {
        res.send('Hello World!');
    });

    app.listen(3000, function () {
        console.log('Example app listening on port 3000!'); 
    });
}