export VERSION=$(cat version)
export STACKNAME="lnxchallenge"

docker stack deploy -c docker-compose.deploy.yml $STACKNAME
