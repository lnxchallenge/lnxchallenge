# lnxchallenge

** Importante, executar os scripts como "sudo" para que não haja nenhum problema.

1 - Se você estiver instalando essa stack em uma VM Linux com nada instalado, por favor execute o script "install-dependencies.sh" para que você possar utilizar os próximos scripts.

2 - Após instalar as dependencias, execute o script "build.sh"

3 - A stack foi criada com docker swarm, é necessario inicilizar o swarm na maquina virtual que será utilizada, para isso utilize o commando abaixo

Antes de executar o "deploy.sh" deverá executar o comando abaixo: 
	
	- docker swarm init --advertise-addr IP ADDRESS

*IP ADDRESS - endereço IP da máquina que está rodando a aplicação*

4 - Execute o script "deploy.sh"

5 - A aplicação deve ser acessada no endereço IP mencionado no passo 3.

6 - Ao usar o script para Rollback será necessário informar a versão que deseja voltar, para que seja feito um rollback de forma segura.

Exemplo de resposta: 1.0.0 / 0.5.0

7 - O teste de carga é feito com o comando "npm test" que busca dentro do package.json o script test e executa de acordo com os parametros passados.
OBS: é necessario mudar o endereço IP dentro do "index.js" para que o teste seja realizado com sucesso.
