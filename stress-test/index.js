const loadtest = require('loadtest');
const options = {
    url: 'http://192.168.1.107:8080',
    maxRequests: 1000,
};
loadtest.loadTest(options, function(error, result){
    if (error){
        return console.error('Got an error: %s', error);
    }
    console.log('Tests run successfully');
    console.log(result)
});
